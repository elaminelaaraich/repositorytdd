package com.nespresso.exercise.waiter;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class SameDish extends Dish {
	public SameDish(String plat) {
		super(plat);
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void addPlat(String name, LinkedHashMap<String, Dish> commands) {
		Set<?> set = commands.entrySet();
		Iterator<?> iterator = set.iterator();
		Map.Entry map = null ;
		while(iterator.hasNext()){
			 map =(Map.Entry)iterator.next();
		}
		commands.put(name, (Dish) map.getValue());
	}
}
