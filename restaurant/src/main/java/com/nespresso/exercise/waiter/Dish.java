package com.nespresso.exercise.waiter;

import java.util.LinkedHashMap;

public abstract class Dish {

	protected String plat;

	public Dish(String plat) {
		this.plat = plat;
	}

	public abstract void addPlat(String name,
			LinkedHashMap<String, Dish> commands); 

}
