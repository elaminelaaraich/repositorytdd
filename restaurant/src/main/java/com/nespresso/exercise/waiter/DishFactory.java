package com.nespresso.exercise.waiter;

public class DishFactory {

	public static Dish createDish(String plat) {
		if (plat.equals("Same"))
			return new SameDish(plat);
		else if (plat.contains(" for ")) {
			return new MultipleDish(plat);

		} else
			return new NormalDish(plat);

	}

}
