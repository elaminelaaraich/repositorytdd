package com.nespresso.exercise.waiter;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class RestaurantView {

	public String parsePlat(String commandCustomer) {
		return getSplit(commandCustomer, 1);

	}

	public String parseCustomer(String commandCustomer) {
		return getSplit(commandCustomer, 0);
	}

	private String getSplit(String commandCustomer, int index) {
		String[] split = commandCustomer.split(": ");
		return split[index];
	}

 

	// il faut voir la ligne : builder.append(", "+ ((Dish)map.getValue()).plat);
	@SuppressWarnings("rawtypes")
	public String print(LinkedHashMap<String, Dish> commands) {
		StringBuilder builder=new StringBuilder();
		Set<?> set = commands.entrySet(); 
		Iterator<?> iterator=set.iterator();
		Map.Entry map = null ;
		while(iterator.hasNext()){
			 map =(Map.Entry)iterator.next();
			 builder.append(", "+ ((Dish)map.getValue()).plat);
		}
		
		 
		 return builder.substring(2);
	}

	public String printMissing(int missingDemmand) {

		return "MISSING "+missingDemmand;
	}

	public String printMissingMultiple(int numberOfPlatMissing,String plat) {
		
		return "MISSING "+numberOfPlatMissing+" for "+plat;
	}

}
