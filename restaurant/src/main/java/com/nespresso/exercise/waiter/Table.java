package com.nespresso.exercise.waiter;


import java.util.LinkedHashMap;
import java.util.PriorityQueue;
import java.util.Queue;

public class Table {

	private int customerNumber;
	private Queue<String> queueCommands = new PriorityQueue<String>();
	private LinkedHashMap<String, Dish> commands=new LinkedHashMap<String, Dish>();

	public Table(int customerNumber) {
		this.customerNumber = customerNumber;

	}

	 

	public void getIformationsFromCommand(String command) {

		queueCommands.offer(command);

	}

	public void addPlat(RestaurantView restaurantView) {
		String command = queueCommands.poll();
		String name = restaurantView.parseCustomer(command);
		String plat = restaurantView.parsePlat(command);
		Dish dish = DishFactory.createDish(plat);
		dish.addPlat(name,commands);
	}



	public String createOrder(RestaurantView restaurantView) {
		 
		if(MultipleDish.isMissing()) {
			return createOrderMultipleDish(restaurantView);
		}
		else if(commands.size()==customerNumber){
			return createOrderNormal(restaurantView);
		}
		else {
			return createOrderMissing(restaurantView);						
		}
				
	}



	private String createOrderMissing(RestaurantView restaurantView) {
		return restaurantView.printMissing(customerNumber-commands.size());
	}



	private String createOrderNormal(RestaurantView restaurantView) {
		return restaurantView.print(commands);
	}



	private String createOrderMultipleDish(RestaurantView restaurantView) {
		return MultipleDish.printFailed(restaurantView) ;
	}
}
