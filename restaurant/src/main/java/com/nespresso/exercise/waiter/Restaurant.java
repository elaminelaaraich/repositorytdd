package com.nespresso.exercise.waiter;

import java.util.ArrayList;
import java.util.List;

public class Restaurant {
	
	List<Table> tables=new ArrayList<Table>();

	public int initTable(int customerNumber) {
		tables.add(new Table(customerNumber));
		int tableId=tables.size()-1;
		return tableId;
	}

	public void customerSays(int tableId, String commandCustomer) {
		RestaurantView restaurantView=new RestaurantView(); 
		tables.get(tableId).getIformationsFromCommand(commandCustomer);
		tables.get(tableId).addPlat(restaurantView);
		
	}

	public String createOrder(int tableId) {
		RestaurantView restaurantView=new RestaurantView(); 
		return tables.get(tableId).createOrder(restaurantView);
	}

}
