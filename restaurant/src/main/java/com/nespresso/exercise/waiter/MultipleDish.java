package com.nespresso.exercise.waiter;

import java.util.LinkedHashMap;

public class MultipleDish extends Dish {

    private static int numberOfMissingPlat = 0;
    private static String namePlat;

    public MultipleDish(String plat) {
        super(plat);
        namePlat = plat;
    }

    @Override
    public void addPlat(String name, LinkedHashMap<String, Dish> commands) {

        if (numberOfMissingPlat != 0)
            numberOfMissingPlat--;
        else
            numberOfMissingPlat = Integer.parseInt(super.plat.split(" for ")[1]) - 1;
        commands.put(name, this);

    }

    public static String printFailed(RestaurantView restaurantView) {
        return restaurantView.printMissingMultiple(numberOfMissingPlat, namePlat);

    }
    public static boolean isMissing() {
        return numberOfMissingPlat!=0;

    }
    

}
