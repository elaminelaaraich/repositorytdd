package com.nespresso.exercise.waiter;

import java.util.LinkedHashMap;

public class NormalDish extends Dish{
	
	public NormalDish(String plat) {
		super(plat);
	}

	@Override
	public void addPlat(String name, LinkedHashMap<String, Dish> commands) {
		commands.put(name, this);
				
	}

	
	
}
